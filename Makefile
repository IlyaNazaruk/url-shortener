# Makefile

current_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

run: install start

install: env-prepare docker-build docker-initialize-mysql docker-up php-install-composer-packages php-env-prepare php-run-migrations vue-build start

env-prepare:
	cp -n .env.example .env

docker-build:
	docker-compose build

docker-up:
	docker-compose up -d

docker-initialize-mysql:
	docker-compose up -d mysql
	docker run --rm --network url_shortener_net jwilder/dockerize -wait tcp://mysql:3306 -timeout 30s

php-install-composer-packages:
	docker exec url_shortener_php composer install

php-env-prepare:
	docker exec url_shortener_php cp -n .env.example .env
	docker exec url_shortener_php php artisan key:generate --ansi

php-run-migrations:
	docker exec url_shortener_php php artisan migrate

vue-build:
	docker run --rm --workdir=/var/www/src -v ${current_dir}/src:/var/www/src node:alpine sh -c "yarn install && yarn dev"

start:
	docker-compose up -d
	echo "Open http://127.0.0.1:8080"

stop:
	docker-compose stop

clean:
	docker-compose stop
	docker-compose run --rm php sh -c "rm -rf .env vendor bootstrap/cache/* node_modules public/css/* public/js/* public/mix-manifest.json storage/framework/cache/data/* storage/framework/views/*.php storage/logs/*.log"
	docker-compose down -v
