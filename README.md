# ShortLink service

## Prerequisites:
GIT, docker, make should be installed before

Make tool also good to have

## Installation:
```bash
make run
```

Put a proper value to ./src/.env GOOGLE_SAFE_BROWSING_API_KEY variable

(Or just check sequence of commands in the Makefile)

## Cleaning up:
```bash
make clean
```

## Access
http://localhost:8080